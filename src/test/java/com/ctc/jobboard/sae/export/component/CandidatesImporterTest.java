package com.ctc.jobboard.sae.export.component;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.sae.component.SAEAccount;
import com.ctc.jobboard.sae.component.SAEOAuth;
import com.ctc.jobboard.sae.domain.Candidate;

public class CandidatesImporterTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		SAEAccount account = new SAEAccount(SAEOAuth.TEST_ADVERTISER_ID, "", "29762681");
		
		CandidatesImporter im = new CandidatesImporter(account);
		List<Candidate> candidates = im.importCandidates();
		
	}

}

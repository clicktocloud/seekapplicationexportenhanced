package com.ctc.jobboard.sae.export.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.sae.component.SAEAccount;
import com.ctc.jobboard.sae.domain.Candidate;
import com.ctc.jobboard.sae.domain.ExtraField;
import com.ctc.jobboard.sae.domain.SeekScreenIdConfig;
import com.ctc.jobboard.sae.export.domain.SAEOrg;
import com.ctc.jobboard.sae.export.util.SAEExportConfig;
import com.sforce.soap.partner.DescribeSObjectResult;
import com.sforce.soap.partner.Field;
import com.sforce.soap.partner.FieldType;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

public class SalesforceService {
	static Logger logger = Logger.getLogger("com.ctc.seek.export");
	public static final String Error_MESSAGE = "Failed to parse this document. Please load the resume manually against the candidates record. Sorry for any inconvenience this may cause.";

	private static String ns = SAEExportConfig.getNamespace();
	
	private static Map<String,Field[]> sobjectFields = new HashMap<String, Field[]>();
    /**
     * Get all seek accounts info wrapped in a list of SeekAccountConfiguration object.
     * Seek account info including seek account, seek max job application Id and seek question Ids
     * @param sforg
     * @return
     */
    public static List<SAEAccount> getAllSeekAccountConfigurations(SAEOrg sforg) throws Exception{
        //a list of SeekAccountConfiguration object to wrap seek account Ids, max job application Id and question Ids
        List<SAEAccount> seekAccountsConfigList = new ArrayList<SAEAccount>();

        // get all seek account Ids
        List<String> seekAccountIdsList = SalesforceHelper.getAllSeekAccountIdsAndSFIds(sforg);

        // get all seek screen Ids
        List<SeekScreenIdConfig> listSeekScreenIdConfig = SalesforceHelper.getSeekScreenIdConfig(sforg);

        // generate list of seek account configurations
        for(String seekAccountAndSFId: seekAccountIdsList){
            String accountId = seekAccountAndSFId.split(":")[0];
            String sfId = seekAccountAndSFId.split(":")[1];
            String maxJobapplicationId = "";
            try{
            		// get max application id from corresponding seek account
            		maxJobapplicationId = SalesforceHelper.getMaxJobapplicationId(accountId,sfId,sforg);
                logger.info("seek account:" + accountId + " -----> max Job Application Id:" + maxJobapplicationId);
            }catch(Exception ex){
            	logger.error("Sorg: " + sforg.getSfusername() + ". Cannot determine max job application id.");
            	continue;
            }
            

            SAEAccount config = new SAEAccount(listSeekScreenIdConfig,maxJobapplicationId, accountId);
            config.setOrgUsername(sforg.getSfusername());
            config.setBucket(sforg.getS3Folder());
            config.setOrgId(sforg.getId15());
            
            seekAccountsConfigList.add(config);
        }

        return seekAccountsConfigList;
    }


   
	public static void setExtraFieldsType(PartnerConnection conn, Candidate candidate, String sobjectName){
		String orgId = loadSObjectFields(conn, sobjectName);
		
        List<ExtraField> listExtraFields = candidate.getListExtraFields();
        if (listExtraFields != null) {
            for(ExtraField extraField : listExtraFields){
            	
            	extraField.setType(getFieldType(  orgId,  sobjectName,extraField.getApiname()));
                
            }
        }
	}
	
	public static FieldType getFieldType(String orgId, String sobjectName, String apiName){
		orgId = orgId == null ? "" : orgId;
		Field[] fields  =  sobjectFields.get(orgId + ":" + sobjectName);
		for(Field f : fields){
			if(f.getName().equalsIgnoreCase(apiName)){
				return f.getType();
			}
		}
		return null;
	}
   
    /**
     * insert questions and answers
     *
     *
     * @param conn
     * @param successfulCandidates
     * @param bucketname
     * @param username  @throws ConnectionException
     */
    public static void insertSelectionCriteria(PartnerConnection conn, List<Candidate> successfulCandidates, String bucketname, String username) throws ConnectionException {
		logger.debug("start to create selection criteria ...");
		// insert Selection Criteria
		List<SObject> selectionCriterias = new ArrayList<SObject>();
		for(Candidate candidate : successfulCandidates){
			if(StringUtils.isNotBlank(candidate.getSelectionCriteriaName())){
				SObject sobj = new SObject();
				sobj.setType(""+ns+"Web_Document__c");
				
				if(candidate.getSelectionCriteriaOriginalName()!=null && candidate.getSelectionCriteriaOriginalName().length()>79){
					sobj.setField("Name", candidate.getSelectionCriteriaOriginalName().substring(0, 78));
				}else{
					sobj.setField("Name", candidate.getSelectionCriteriaOriginalName());
				}
				
				sobj.setField(""+ns+"ObjectKey__c", candidate.getSelectionCriteriaName());
				sobj.setField(""+ns+"S3_Folder__c", bucketname);
				sobj.setField(""+ns+"Type__c", "CoverLetter");
				sobj.setField(""+ns+"Document_Related_To__c",candidate.getId());
				sobj.setField(""+ns+"File_Size__c", candidate.getSelectionCriteriaSize());
				selectionCriterias.add(sobj);
			}else{
				// do nothing
			}
		}

		SaveResult[] sr_SelectionCriteria = SalesforceHelper.massCreate(conn, selectionCriterias.toArray(new SObject[]{}));
		
		for(int i=0; i<sr_SelectionCriteria.length;i++){
			if (!sr_SelectionCriteria[i].isSuccess()) {
				com.sforce.soap.partner.Error[] selectionCriteriaerrors = sr_SelectionCriteria[i].getErrors();
				for (com.sforce.soap.partner.Error selectionCriteriaerror : selectionCriteriaerrors) {
					logger.error(username + ": insert Selection Criterias "+Arrays.toString(selectionCriteriaerror.getFields()) + " : "+ selectionCriteriaerror.getMessage() + " candidate info: "+ successfulCandidates.get(i));
				}
			}
		}	
		logger.debug("end of creating selection criteria ...");
	}

    /**
     * insert candidate management records
     *
     *
     * @param sforg
     * @param conn
     * @param successfulCandidates
     * @throws ConnectionException
     */
    public static void insertCandidateManagement(SAEOrg sforg, PartnerConnection conn, List<Candidate> successfulCandidates) throws ConnectionException {
		logger.debug("start to create candidate management ...");
        String username = sforg.getSfusername();

        List<SObject> cmListToCreate = new ArrayList<SObject>();
        for(Candidate can:successfulCandidates){

            // check if the vacancy has been deleted
            if(can.getVacancyId() == null ){
                logger.error(username + ": Candidate management create failed for candidate " + can.getId() + ". Ad "+ can.getAdvertisementId() + " was missing.");
                continue;
            }

            SObject cm = new SObject();
            cm.setType(""+ns+"Placement_Candidate__c");
            cm.setField(""+ns+"Candidate__c", can.getId());
            cm.setField(""+ns+"Online_Ad__c", can.getAdvertisementId());
            cm.setField(""+ns+"Placement__c", can.getVacancyId());
            cm.setField(""+ns+"Status__c", "New");

//            if(sforg.isMultiJobPostingAccounts()){
//                cm.setField(""+ns+"Job_Application_Id__c",can.getJobApplicationId());   // insert seek job application id
//                
//                cm.setField(""+ns+"Job_Application_Ext_Id__c",can.getJobApplicationId());   // new job application id (external id)
//            }
            cm.setField(""+ns+"Job_Application_Id__c",can.getJobApplicationId());   // insert seek job application id
            cm.setField(""+ns+"Job_Application_Ext_Id__c",can.getJobApplicationId());   // new job application id (external id)
            cmListToCreate.add(cm);
        }

        SaveResult[] cmResults = null;
        try{
            cmResults = SalesforceHelper.massCreate(conn, cmListToCreate.toArray(new SObject[]{}));
        }catch(Exception ex){
            logger.error(username + " exceptions when inserting candidate managements.");
            return;
        }

		
		for(int i=0; i<cmResults.length; i++){
			if(cmResults[i].isSuccess()){
				logger.debug("successful candidate management" + (i+1) +":" +  cmResults[i].getId() );
			}
			if(!cmResults[i].isSuccess()){
				com.sforce.soap.partner.Error[] cmserrors = cmResults[i].getErrors();
				for(com.sforce.soap.partner.Error cmerror : cmserrors){
					logger.error(username + ": insert Candidate Management "+Arrays.toString(cmerror.getFields())+" : "+cmerror.getMessage()+" candidate info: "+successfulCandidates.get(i));
				}
			}
		}	
		logger.debug("end of creating candidate management ...");
	}


    /**
     * set fields before inserting candidates
     *
     * @param sobj
     * @param candidate
     */
    public static void setContactFields (SObject sobj, Candidate candidate) {
        sobj.setField("LastName", candidate.getLastname());
        sobj.setField("FirstName", candidate.getFirstname());
        sobj.setField("Email", candidate.getEmail());
        sobj.setField("Phone", candidate.getPhone());
        sobj.setField(""+ns+"Candidate_From_Web__c", true);
        sobj.setField(""+ns+"jobApplication_Id__c", candidate.getJobApplicationId());
        sobj.setField(""+ns+"jobseek_id__c", candidate.getJobseekerId());
        sobj.setField(""+ns+"job_reference__c", candidate.getJobreference());
        sobj.setField(""+ns+"Date_Submitted__c", candidate.getDateSubmitted());
        sobj.setField(""+ns+"Resume_Source__c", candidate.getResumeOriginalName());
        sobj.setField(""+ns+"Resume__c", candidate.getResumeContent());

        //###########  New added by Gilberto
        List<ExtraField> listExtraFields = candidate.getListExtraFields();
        if (listExtraFields != null) {
            for(ExtraField extraField : listExtraFields){
            	
            	sobj.setField(extraField.getApiname(), extraField.getConvertedValue());

            }
        }
        //###########  End of New added by Gilberto
    }
    
  
    
    
	public static String loadSObjectFields(PartnerConnection conn, String objectName) {
		String key = objectName;
		String orgId = "";
		try {
			orgId = conn.getUserInfo().getOrganizationId();
			orgId = orgId == null ?"" :orgId;
			
			key = orgId + ":" + objectName;
		} catch (ConnectionException e) {
			logger.error(e);
		}
		
		Field[] fields = sobjectFields.get(key);
		if(fields != null){
			logger.debug(key + "'s fields are existing in memory !");
			return orgId;
		}

	    try {
	
	       
	        DescribeSObjectResult describeSObjectResult = conn.describeSObject(objectName);
	
	        // Get sObject metadata
	        if (describeSObjectResult != null) {
	        	// Get the fields
	        	
	        	fields = describeSObjectResult.getFields();
	        	
	        	sobjectFields.put(key, fields);

	      }
	
	    } catch (ConnectionException ce) {
	    	logger.error(ce);
	    }
	    
	    return orgId;
	
	}
    
    
}
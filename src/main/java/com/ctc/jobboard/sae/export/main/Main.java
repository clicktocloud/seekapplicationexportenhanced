

package com.ctc.jobboard.sae.export.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.ctc.jobboard.sae.export.domain.SAEOrg;
import com.ctc.jobboard.sae.export.helper.SalesforceHelper;
import com.ctc.jobboard.sae.export.service.SeekApplicationExportRunnable;
import com.ctc.jobboard.util.SfOrg;

/**
 * @author Kevin
 * 
 * The main method generates two threads to simultaneously deal with all SAE clients one by one.
 *
 */
public class Main {

	static Logger logger = Logger.getLogger("com.ctc.seek.export");
	private static final int NUMBER_OF_THREADS = 4;

	public static void main(String[] args) {
		// list of SAE orgs
		List<SAEOrg> orgsList = new ArrayList<SAEOrg>();
		
		try {
			logger.info(" ====================  Start of Seek Application Export   =======================");
			if(args.length>0){
				// to get a single client
				logger.info("running SAE for a single client:" + args[0]);
				orgsList = SalesforceHelper.getExportList(args[0]);
				if(orgsList.size()==0){
					logger.info("The client does not exist. Please check the username or SAE settings on corporate instance.");
					return;
				}
			}else{
				// to get all clients
				orgsList = SalesforceHelper.getExportList("all");
				logger.info("running SAE for all "+ orgsList.size() +" clients.");
			}
			
			
			//get 4 orgsList for 4 thread.
			
			// created threads 
			for(int i = 0 ; i < NUMBER_OF_THREADS ; i++){
			    SeekApplicationExportRunnable saer = new SeekApplicationExportRunnable(orgsList);
				Thread saet = new Thread(saer);
				saet.setName("SAE_THREAD_" + (i+1));
				saet.start();
			}
		} catch (Exception e) {
			logger.error("SAE encountered errors. " + e);
			
		} 
	}

}

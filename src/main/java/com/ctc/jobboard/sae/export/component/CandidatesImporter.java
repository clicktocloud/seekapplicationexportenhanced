package com.ctc.jobboard.sae.export.component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.entity.FilePackage;
import com.ctc.entity.Sforg;
import com.ctc.jobboard.sae.component.SAEAccount;
import com.ctc.jobboard.sae.component.SAEApplicationsResponseHandler;
import com.ctc.jobboard.sae.component.SAECandidatesRetriever;
import com.ctc.jobboard.sae.component.SAEJobBoard;
import com.ctc.jobboard.sae.domain.Candidate;
import com.ctc.jobboard.util.SfOrg;
import com.ctc.people.cfm.entity.CTCFile;
import com.ctc.people.cfm.entity.CTCFilesOwner;
import com.ctc.people.cfm.entity.Constant;
import com.ctc.people.cfm.entity.JBField;
import com.ctc.people.cfm.entity.JBObject;
import com.ctc.people.cfm.exception.ResourceDownloadException;
import com.ctc.people.cfm.extension.service.CandidateFilesService;

public class CandidatesImporter {
	
	static Logger logger = Logger.getLogger(CandidatesImporter.class);

	
	private SAEAccount account;
	
	public CandidatesImporter(SAEAccount account){
		this.account = account;
	}
	
	
	public List<Candidate>  importCandidates(){
		//retrieve candidates from seek job board
		SAEJobBoard jobboard = new SAEJobBoard("seek");
    	jobboard.setAccount(account);
    	jobboard.setOperator(account.getOrgUsername());
    	jobboard.setBucket(account.getBucket());
    	
    	jobboard.makeJobFeeds();
    	
    	List<Candidate> candidates =( (SAEApplicationsResponseHandler)jobboard.getResponseHandler()).getResult();
		
		if( candidates == null )
			return null;
		
		//import candidates to salesforce and s3 server
    	for(Candidate candidate : candidates){
    		CTCFilesOwner fileOwner = buildFilesOwner(whoMap, candidate, orgInfo,skillGroupString);
			if(fileOwner == null){
				logger.warn("Failed to build file owner !");
				
				continue;
			}
			
			CandidateFilesService.make(fileOwner);
			
    	}
    	
    		
    	return  candidates;
	}
    	
    	private CTCFilesOwner buildFilesOwner(Map<String, String> whoMap, Candidate candidate ,  Sforg orgInfo, String skillGroupString){
    		
    		CTCFilesOwner filesOwner = null;
    			
    		if( candidate == null  )
    			return filesOwner;
    				
    		try {
    			
    			String ns = orgInfo.getNamespace()==null?"":orgInfo.getNamespace();
    			
    			filesOwner = new CTCFilesOwner();
    			
    			
    		
    			
    			//set properties values of CTCFilesOwner object by using ActivityDoc object
    			String whoId = "";
    			String whoIdField = "";
    			
    			if(whoMap!= null && whoMap.size()>0) {
    				for(String Id : whoMap.keySet()) {
    					whoId = Id;
    					whoIdField = whoMap.get(Id);
    				}
    			}
    			
    			String ownerId = whoId;
    			
    					
    			filesOwner.setOwnerId(  ownerId );
    			filesOwner.setOrgId(  orgInfo.getOrgid());
    			filesOwner.setBucketName( orgInfo.getBucketName());
    			filesOwner.setWhoId( whoId);
    			filesOwner.setWhoIdField( whoIdField);
    			
    			String namespace = orgInfo.getNamespace() == null ? "" : orgInfo.getNamespace();
    			filesOwner.setNamespace( namespace);
    			
    			logger.debug("ownerId : " + ownerId);
    			logger.debug("orgId : " + orgInfo.getOrgid());
    			logger.debug("bucketName : " + orgInfo.getBucketName());
    			logger.debug("whoId : " + whoId);
    			logger.debug("whoIdField : " + whoIdField);
    			
    			if(uploadedFiles.getItemist() != null){
    				//create CTCFile object for each resource and assign it to the CTCFilesOwner object
    				for(FilePackage.Item fileItem  : uploadedFiles.getItemist()){
    					
    					
    					//create local temp file for each downloaded resource from salesforce
    					CTCFile ctcFile= new CTCFile();
    					
    				
    					
    					logger.debug("----------------------------------");
    					logger.debug(ctcFile.getMetadata().getObjectName());
    					for(JBField field : ctcFile.getMetadata().getFields()){
    						logger.debug(field.getName() + " = " + field.getValue());
    						
    					}
    					
    					logger.debug("----------------------------------");
    					
    					String name = (String) fileItem.metadata.get("Name");
    					String newName = (String) fileItem.metadata.get(ns+"ObjectKey__c");
    					String path = fileItem.file.getAbsolutePath();   
    					String docType = (String) fileItem.metadata.get(ns+"Type__c");
    					String size = (String) fileItem.metadata.get(ns+"File_Size__c");
    					
    					logger.debug("---------------------");
    					logger.debug("filename : " + name);
    					logger.debug("path : " + path);
    					logger.debug("docType : " + docType);
    					logger.debug("size : " + size);
    					
    					
    					
    					
    					
    					ctcFile.setResourceId( "" );
    					ctcFile.setOriginalFileName(name);
    					ctcFile.setNewFileName(newName);
    					ctcFile.setPath(path);
    					ctcFile.setType(docType);
    					ctcFile.setSize(size);
    					
    					filesOwner.getFiles().add( ctcFile );	
    					
    					if(docType.equalsIgnoreCase( Constant.RESUME_DOC_TYPE) ) {  
    						
    						if(skillGroupString != null && !skillGroupString.equals("")){
    							List<String> skillGroupIds = Arrays.asList( skillGroupString.split(":") );
    							filesOwner.setSkillGroups(skillGroupIds);
    							filesOwner.setEnableSkillParsing( true);
    						}else{
    							filesOwner.setEnableSkillParsing( false);
    						}		
    						
    						logger.debug("skillGroupIds : " + skillGroupString);
    						
    					}
    					
    					ctcFile.setOwner(filesOwner);
    					
    				}
    			}
    			
    			
    		} catch (Exception e) {
    			logger.error("Failed to fill CTCFilesOwner object." + e );
    			throw new ResourceDownloadException("Failed to fill CTCFilesOwner object." + e);
    		} 
    		
    		return filesOwner;
    	
    	}

}

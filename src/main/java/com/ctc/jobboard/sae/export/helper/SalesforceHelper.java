package com.ctc.jobboard.sae.export.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.sae.domain.Candidate;
import com.ctc.jobboard.sae.domain.SeekScreenIdConfig;
import com.ctc.jobboard.sae.export.domain.SAEOrg;
import com.ctc.jobboard.sae.export.exeption.SeekApplicationException;
import com.ctc.jobboard.sae.export.util.SAEExportConfig;
import com.ctc.jobboard.util.BasicConfig;
import com.ctc.salesforce.auth.SalesforceCredential;
import com.ctc.salesforce.service.SalesforceConnector;
import com.sforce.soap.partner.DeleteResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

public class SalesforceHelper {
	static Logger logger = Logger.getLogger("com.ctc.seek.export");
	static String ns;
	
	static{
		  ns =SAEExportConfig.getNamespace();
		
	}
	

	/**
	 * Get connection to salesforce
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws ConnectionException
	 */
	
	public static PartnerConnection login(String orgId){
		SalesforceCredential credential = new SalesforceCredential();
        credential.setPasswordLogin(false);
        credential.setOrgId(orgId);
        PartnerConnection conn = null;
        try {
            SalesforceConnector connector = new SalesforceConnector(credential);
            conn = connector.getPartnerConnection();
        } catch (ConnectionException e) {
        		logger.error("Cannot setup the OAuth connection for org: " + orgId + ". ", e);
			throw new SeekApplicationException("ConnectionException : orgId=" + orgId + ". "+ e );
        }
        if (conn == null) {
    			logger.error("Cannot setup the OAuth connection for org: " + orgId + ". ");
    			throw new SeekApplicationException("ConnectionException : orgId=" + orgId + ". ");
        }
        return conn;
	}	

	public static List<SAEOrg> getExportList(String username) throws Exception {
		String corporate_orgId = BasicConfig.CORP_ORG_ID;
		PartnerConnection conn = login(corporate_orgId);
		
		String query = "select SFDC_Org_ID__c, Name, S3_Account__c, S3_Password__c, Total_Licenses__c, "
				+ "SF_UserName__c, S3_Folder__c, Enable_Upload_Other_Files__c, Use_18_Digit_as_Job_Ref__c "
				+ "from Account where S3_Account__c!=null "
				+ " and Enable_SAE__c=true ";
		
		

		if (username.equals("all")) {
			// query to get all clients.
			// do nothing
		} else {
			// query to get a single client
			query = query + " and SF_UserName__c='" + username + "'";
		}
		
		query = query + " order by Total_Licenses__c desc";

		QueryResult qr = conn.query(query);
		List<SAEOrg> list = new ArrayList<SAEOrg>();
		boolean done = false;
		if (qr.getSize() > 0) {
			while (!done) {
				for (SObject sobj : qr.getRecords()) {
					SAEOrg sforg = new SAEOrg();
					sforg.setId15(sobj.getField("SFDC_Org_ID__c").toString());
					sforg.setName(sobj.getField("Name").toString());
					sforg.setSfusername(sobj.getField("SF_UserName__c")
							.toString());
					sforg.setTotalLicenseNumber(sobj.getField("Total_Licenses__c").toString());

					// if the S3 bucket, S3 password or the S3 account name is
					// empty on corporate instance, the client will be skipped
					// and an error will be sent to development team by email.
					if (sobj.getField("S3_Password__c") == null) {
						logger.error(sobj.getField("Name").toString()
								+ ",username:"
								+ sobj.getField("SF_UserName__c").toString()
								+ " does not provide S3 password in corporate instance.");
						continue;
					}
					if (sobj.getField("S3_Folder__c") == null) {
						logger.error(sobj.getField("Name").toString()
								+ ",username:"
								+ sobj.getField("SF_UserName__c").toString()
								+ " does not provide a valid S3 bucket in corporate instance.");
						continue;
					}

					// if the client is using 'upload other files' function
					boolean uploadOtherFilesEnabled = false;
					if (sobj.getField("Enable_Upload_Other_Files__c")
							.toString().equalsIgnoreCase("true")) {
						uploadOtherFilesEnabled = true;
					} else {
						// do nothing
					}

					// if the client is using 18 digit ad id for job reference
					// number
					boolean use18DigitAdId = false;
					if (sobj.getField("Use_18_Digit_as_Job_Ref__c") != null
							&& sobj.getField("Use_18_Digit_as_Job_Ref__c")
									.toString().equalsIgnoreCase("true")) {
						use18DigitAdId = true;
					} else {
						// do nothing
					}

					sforg.setUse18DigitAdIdForJobRefNumber(use18DigitAdId);
					sforg.setUploadOtherDocEnabled(uploadOtherFilesEnabled);
					sforg.setS3accesskey(sobj.getField("S3_Account__c")
							.toString());
					sforg.setS3secretkey(sobj.getField("S3_Password__c")
							.toString());
					sforg.setS3Folder(sobj.getField("S3_Folder__c").toString());

					list.add(sforg);
				}
				if (qr.isDone()) {
					done = true;
				} else {
					qr = conn.queryMore(qr.getQueryLocator());
				}
			}
		}
		return list;
	}

	/**
	 * This method aims to get a single client from the list and remove the
	 * client from the queue.
	 * 
	 * @param allSAEOrgs
	 * @return
	 */
	public synchronized static SAEOrg getSingleOrg(List<SAEOrg> allSAEOrgs) {
		SAEOrg sforg = null;
		if (!allSAEOrgs.isEmpty()) {
			sforg = allSAEOrgs.get(0);
			allSAEOrgs.remove(0);
		}
		return sforg;
	}

	/**
	 * Get all seek account Ids
	 * 
	 * @param sforg
	 * @return
	 */
	public static List<String> getAllSeekAccountIdsAndSFIds(
			SAEOrg sforg) {
		List<String> seekAccountIdsList = new ArrayList<String>();
		try {
			seekAccountIdsList = getMultiAccountIds(sforg.getId15(), sforg.getSfusername());
			logger.debug("all seek account ids:" + seekAccountIdsList);
			return seekAccountIdsList;
		} catch (Exception ex) {
			logger.error(sforg.getSfusername()
					+ " failed get seek account ids.");
			throw new SeekApplicationException(sforg.getSfusername()
					+ " failed to get seek account ids.");
		}
	}

	/**
	 * Get multiple seek accounts ids Return value is
	 * seekAccountId:SfId
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	private static List<String> getMultiAccountIds(String orgId, String username) {
		
		logger.debug("getting multiple seek account ids....");
		List<String> accountIdsList = new ArrayList<String>();
		try {
			PartnerConnection conn = login(orgId);
			
//			QueryResult qr = conn
//					.query("select Id,"+ns+"Advertiser_Id__c from "+ns+"StyleCategory__c "
//							+ "where "+ns+"Account_Active__c=true and RecordType.DeveloperName ='Seek_Account'");
			
			QueryResult qr = conn
					.query("select Id,"+ns+"Advertisement_Account__c from "+ns+"Advertisement_Configuration__c "
							+ "where "+ns+"Active__c=true and RecordType.DeveloperName ='Seek_Account'");

			if (qr.getSize() > 0) {
				for (int i = 0; i < qr.getSize(); i++) {

					if (qr.getRecords()[i]
							.getField(ns+"Advertisement_Account__c") != null
							&& qr.getRecords()[i].getField(
									ns+"Advertisement_Account__c")
									.toString() != "") {
						
						logger.debug("seek account:"
								+ qr.getRecords()[i].getField(
										ns+"Advertisement_Account__c")
										.toString() + ";SF id:"
								+ qr.getRecords()[i].getId());
						accountIdsList.add(qr.getRecords()[i].getField(
								ns+"Advertisement_Account__c").toString()
								+ ":" + qr.getRecords()[i].getId());
					} else {
						logger.error(username + " has no valid seek account.");
						throw new SeekApplicationException(username
								+ " has no valid seek account.");
					}
				}
			} else {
				logger.error(username + " has no valid seek account.");
				throw new SeekApplicationException(username
						+ " has no valid seek account.");
			}
		} catch (Exception ex) {
			logger.error(ex);
			logger.error(username + " failed to get multi seek account ids.");
			throw new SeekApplicationException(username
					+ " failed to get all seek account ids.");
		}
		return accountIdsList;
	}

	/**
	 * Get seek screen ids from salesforce
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static List<SeekScreenIdConfig> getSeekScreenIdConfig(SAEOrg sforg) {
		
		logger.info("getting seek screen ids....");
		ArrayList<SeekScreenIdConfig> ssList = new ArrayList<SeekScreenIdConfig>();
		try {
			PartnerConnection conn = login(sforg.getId15());
//			QueryResult qr = conn
//					.query("select Name, "+ns+"Question_Id__c, "+ns+"StyleCategory__r."+ns+"screenId__c from "+ns+"Seek_Screen_Field__c "
//							+ " where  "+ns+"StyleCategory__r."+ns+"Active__c=true and "+ns+"StyleCategory__r.RecordType.DeveloperName ='seekScreenStyle' ");
			
			QueryResult qr = conn
					.query("select Name, "+ns+"Questions__c, "+ns+"Application_Question_Set__r."+ns+"ID_API_Name__c from "+ns+"Application_Question__c "
							+ " where  "+ns+"Application_Question_Set__r."+ns+"Active__c=true and "+ns+"Application_Question_Set__r.RecordType.DeveloperName ='Seek_Screen' ");
		
			
			if (qr.getSize() > 0) {
				for (SObject sobj : qr.getRecords()) {
					SeekScreenIdConfig seekScreenIdConfig = new SeekScreenIdConfig();

					String apiname = sobj.getField("Name").toString();
					
					seekScreenIdConfig.setApiname(apiname);
					// logger.debug("getSeekScreenIdConfig:apiname = "+apiname);

					String questionId = sobj.getField(
							""+ns+"Questions__c").toString();
					seekScreenIdConfig.setQuestionId(questionId);
					// logger.debug("getSeekScreenIdConfig:questionId = "+questionId);

					Object field = sobj.getChild(
							""+ns+"Application_Question_Set__r").getField(
							""+ns+"ID_API_Name__c");
					String seekScreenId = "";
					if (field != null) {
						seekScreenId = field.toString();
					}
					seekScreenIdConfig.setScreenId(seekScreenId);
					// logger.debug("getSeekScreenIdConfig:seekScreenId = "+seekScreenId);

					// logger.debug("getSeekScreenIdConfig:seekScreenIdConfig = "+seekScreenIdConfig);
					ssList.add(seekScreenIdConfig);
				}
			}
		} catch (ConnectionException e) {
			logger.error(sforg.getSfusername() + " cannot get the SeekScreenIdConfig : " + e);
			throw new SeekApplicationException(sforg.getSfusername()
					+ " cannot get the SeekScreenIdConfig : " + e);
		}
		return ssList;
	}

	/**
	 * Get max job application Id from either candidate management or candidate
	 * record according to whether the client is using multi seek accounts
	 * 
	 * @param seekAccountId
	 * @param sforg
	 * @return
	 */
	public static String getMaxJobapplicationId(String seekAccountId,
			String sfId, SAEOrg sforg) {
		try {
			// get the max job application Id from candidate management
			return getMaxJobAppIdFromCandidateManagement(seekAccountId,
					sfId, sforg.getId15(), sforg.getSfusername());
		} catch (Exception ex) {
			throw new SeekApplicationException(
					"Cannot determine max job application id.");
		}

	}

	private static String getMaxJobAppIdFromCandidateManagement(
			String seekAccountId, String sfId, String orgId, String username) {
		PartnerConnection conn = login(orgId);

		
		// Check if there is any job ad in SF instance
		//String queryAdverts = "select Id from "+ns+"Advertisement__c Where "+ns+"Seek_Account__c = '"
		//		+ sfId + "' and isDeleted=false limit 10";
		
		String queryAdverts = "select Id from "+ns+"Advertisement__c Where "+ns+"Advertisement_Account__c = '"
				+ sfId + "' and isDeleted=false limit 10";
		
		QueryResult advertQr;
		try {
			advertQr = conn.query(queryAdverts);
			if (advertQr.getSize() == 0) {
				logger.info(username
						+ " doesn't have any advertisement, no job application would be retrieved");
				return String.valueOf(Integer.MAX_VALUE);
			}
		} catch (ConnectionException e) {
			logger.error(
					"Encounter Error while checking number of job ads for "
							+ username, e);
			throw new SeekApplicationException(
					"Encounter Error while checking number of job ads for "
							+ username, e);
		}

		// There is at least one job in SF, retrieve max candidate id
		// new query using external id Job_Application_Ext_Id__c
		// to get job application id
		String queryString = "Select "+ns+"Job_Application_Ext_Id__c From "+ns+"Placement_Candidate__c "
				+ "Where "+ns+"Job_Application_Ext_Id__c> -1 and "+ns+"Online_Ad__c "
				+ "IN (select Id from "+ns+"Advertisement__c "
				+ "Where "+ns+"Advertisement_Account__c = '"
				+ sfId
				+ "' and isDeleted=false) "
				+ "Order by "+ns+"Job_Application_Ext_Id__c DESC limit 1";

		try {
			QueryResult qr = conn.query(queryString);
			if (qr.getSize() != 0) {
				String result = qr.getRecords()[0].getField(
						""+ns+"Job_Application_Ext_Id__c").toString();
				return removeDecimal(result);
			} else {
				logger.error("Cannot determine max job application id. No valid job application id found on candidate management records. ");
				throw new SeekApplicationException(
						"Cannot determine max job application id. No valid job application id found on candidate management records. ");
			}
		} catch (ConnectionException e) {
			logger.fatal(username
					+ " failed to get max job application id for seek : " + e);
			throw new SeekApplicationException(
					username
							+ " failed to get max job application id for seek application export : "
							+ e);
		} catch( Exception e){
			logger.fatal(username
					+ " failed to get max job application id for seek : " + e);
			throw new SeekApplicationException(
					username
							+ " failed to get max job application id for seek application export : "
							+ e);
		}

	}

	public static String removeDecimal(String source) {
		// logger.info("source = "+source);
		BigDecimal db = new BigDecimal(source);
		return db.intValue() + "";
	}

	
	/**
	 * This method aims to retrieve the salesforce ids by external ids
	 * 
	 * @param skillExtIdList
	 * @param conn
	 * @return
	 * @throws ConnectionException
	 */
	public static ArrayList<String> getSkillSfIds(
			ArrayList<String> skillExtIdList, PartnerConnection conn)
			throws ConnectionException {
		
		QueryResult queryResult = null;
		ArrayList<String> skillSfIdList = new ArrayList<String>();

		// to assemble the condition for the soql query
		String queryCondition = "";
		String query = "";
		for (int i = 0; i < skillExtIdList.size(); i++) {
			if (i == 0 && skillExtIdList.size() == 1) {
				queryCondition = queryCondition + "'" + skillExtIdList.get(i)
						+ "'";

			} else if (i == (skillExtIdList.size() - 1)) {
				queryCondition = queryCondition + "'" + skillExtIdList.get(i)
						+ "'";

			} else {
				queryCondition = queryCondition + "'" + skillExtIdList.get(i)
						+ "', ";
			}
		}

		query = "select Id from "+ns+"Skill__c where "+ns+"Ext_Id__c IN ("
				+ queryCondition + ")";
		queryResult = conn.query(query);
		boolean done = false;
		if (queryResult.getSize() > 0) {
			while (!done) {
				for (SObject sobj : queryResult.getRecords()) {
					skillSfIdList.add(sobj.getField("Id").toString());
				}
				if (queryResult.isDone()) {
					done = true;
				} else {
					queryResult = conn.queryMore(queryResult.getQueryLocator());
				}
			}
		}
		return skillSfIdList;
	}

	/**
	 * Mass create multiple records.
	 * 
	 * @param conn
	 * @param sObjects
	 * @return
	 * @throws ConnectionException
	 */
	public static SaveResult[] massCreate(PartnerConnection conn,
			SObject[] sObjects) throws ConnectionException {
		List<SObject> tmpSObjects = new ArrayList<SObject>();
		List<SaveResult[]> srsList = new ArrayList<SaveResult[]>();
		SaveResult[] srsFinal = new SaveResult[sObjects.length];
		int recordsSize = 0;

		// create new records
		for (SObject sobj : sObjects) {
			tmpSObjects.add(sobj);
			recordsSize++;

			if (tmpSObjects.size() == 200 || recordsSize == sObjects.length) {
				SaveResult[] srsTmp = conn.create(tmpSObjects
						.toArray(new SObject[] {}));
				srsList.add(srsTmp);
				tmpSObjects.clear();
			}
		}

		// generate the SaveResult[]
		int srsIndex = 0;
		for (SaveResult[] srsTmp : srsList) {
			for (int i = 0; i < srsTmp.length; i++) {
				srsFinal[srsIndex] = srsTmp[i];
				srsIndex++;
			}
		}
		return srsFinal;
	}

	/**
	 * mass update multiple records
	 * 
	 * @param conn
	 * @param sObjects
	 * @return
	 * @throws ConnectionException
	 */
	public static SaveResult[] massUpdate(PartnerConnection conn,
			SObject[] sObjects) throws ConnectionException {
		List<SObject> tmpSObjects = new ArrayList<SObject>();
		List<SaveResult[]> srsList = new ArrayList<SaveResult[]>();
		SaveResult[] srsFinal = new SaveResult[sObjects.length];

		int recordsSize = 0;

		// update records
		for (SObject sobj : sObjects) {
			tmpSObjects.add(sobj);
			recordsSize++;

			if (tmpSObjects.size() == 200 || recordsSize == sObjects.length) {
				SaveResult[] srsTmp = conn.update(tmpSObjects
						.toArray(new SObject[] {}));
				srsList.add(srsTmp);
				tmpSObjects.clear();
			}
		}

		// generate the SaveResult[]
		int srsIndex = 0;
		for (SaveResult[] srsTmp : srsList) {
			for (int i = 0; i < srsTmp.length; i++) {
				srsFinal[srsIndex] = srsTmp[i];
				srsIndex++;
			}
		}
		return srsFinal;
	}

	/**
	 * mass delete multiple records
	 * 
	 * @param conn
	 * @param ids
	 * @return
	 * @throws ConnectionException
	 */
	public static DeleteResult[] massDelete(PartnerConnection conn, String[] ids)
			throws ConnectionException {
		List<String> tmpIds = new ArrayList<String>();
		List<DeleteResult[]> srsList = new ArrayList<DeleteResult[]>();
		DeleteResult[] srsFinal = new DeleteResult[ids.length];

		int recordsSize = 0;

		// delete records
		for (String id : ids) {
			tmpIds.add(id);
			recordsSize++;

			if (tmpIds.size() == 200 || recordsSize == ids.length) {
				DeleteResult[] srsTmp = conn.delete(tmpIds
						.toArray(new String[] {}));
				srsList.add(srsTmp);
				tmpIds.clear();
			}
		}

		// generate the SaveResult[]
		int srsIndex = 0;
		for (DeleteResult[] srsTmp : srsList) {
			for (int i = 0; i < srsTmp.length; i++) {
				srsFinal[srsIndex] = srsTmp[i];
				srsIndex++;
			}
		}
		return srsFinal;
	}

	/**
	 * To check whether the salesforce client is using skill parsing
	 * 
	 * @param conn
	 * @param username
	 * @return
	 */
    /*public static boolean isSkillParsingEnabled(PartnerConnection conn,
			String username) {
		String[] skillparsingclients = SalesforceHelper
				.getValueFromPropertyFile("skillparsingclients",
						"information.properties").split(";");
		return Arrays.asList(skillparsingclients).contains(username);
	} /*
	
	 /**
	 * To check whether the salesforce client is using skill parsing
	 * 
	 * @param daxtraAccountService
	 * @param orgId
	 * @return
	 * @Author added by Lorena
	 */
	public static boolean isSkillParsingEnabled(String dasOrgId, String orgId) {
		if(dasOrgId != null && dasOrgId.equals(orgId) ) {
			return true;
		} else {
			return false;
		}
		
	}

	/**
	 * Get infomation from object vacancy from salesforce
	 * 
	 * @param conn
	 * @param successCandidateList
	 * @param skillParsingEnabled
	 * @param username
	 */
	public static void retrieveVacancyInfo(PartnerConnection conn,
			List<Candidate> successCandidateList, boolean skillParsingEnabled,
			SAEOrg sforg) {
		logger.debug("start to retrieve vacancy info ...");
		

		List<String> adIdsList = new ArrayList<String>(); // ad ids list
		Set<String> adIdsSet = new HashSet<String>(); // ad ids set for
														// generating soql query
		Map<String, String> vacanciesMap = new HashMap<String, String>(); // ad
																			// id
																			// as
																			// key,
																			// vacancy
																			// id
																			// as
																			// value
		Map<String, List<String>> skillGroupsCriteriaMap = new HashMap<String, List<String>>(); // skill
																								// group
																								// criteria
																								// map
																								// with
																								// ad
																								// id
																								// as
																								// key
																								// and
																								// list
																								// of
																								// skill
																								// groups
																								// as
																								// value

		// get ads ids and vacancies ids
		for (Candidate can : successCandidateList) {
			adIdsList.add(can.getJobreference().split(":")[1]);
			adIdsSet.add("'" + can.getJobreference().split(":")[1] + "'");
			can.setAdvertisementId(can.getJobreference().split(":")[1]); // set
																			// ad
																			// Id
																			// for
																			// candidate
		}

		// retrieve info from salesforce, meanwhile check if it is using skill
		// parsing
		//
		QueryResult qr = null;
		try {
			if (sforg.isUse18DigitAdIdForJobRefNumber()) {
				if (skillParsingEnabled) {
					// get id, vacancy id and selected skill group
					qr = conn
							.query("select Id, "+ns+"Vacancy__r.Id, "+ns+"Skill_Group_Criteria__c, "+ns+"Use_18_Characters_as_Job_Ref__c from "+ns+"Advertisement__c where Id IN ("
									+ StringUtils.join(adIdsSet, ",") + ")");
				} else {
					qr = conn
							.query("select Id, "+ns+"Vacancy__r.Id, "+ns+"Use_18_Characters_as_Job_Ref__c from "+ns+"Advertisement__c where Id IN ("
									+ StringUtils.join(adIdsSet, ",") + ")");
				}
			} else {
				if (skillParsingEnabled) {
					// get id, vacancy id and selected skill group
					qr = conn
							.query("select Id, "+ns+"Vacancy__r.Id, "+ns+"Skill_Group_Criteria__c from "+ns+"Advertisement__c where Id IN ("
									+ StringUtils.join(adIdsSet, ",") + ")");
				} else {
					qr = conn
							.query("select Id, "+ns+"Vacancy__r.Id from "+ns+"Advertisement__c where Id IN ("
									+ StringUtils.join(adIdsSet, ",") + ")");
				}
			}

		} catch (Exception ex) {
			logger.error(sforg.getSfusername()
					+ ": Error when retrieving vacancy info.");
			return;
		}

		for (SObject sobj : qr.getRecords()) {
			int adDigit = 15;
			if (sobj.getField(""+ns+"Use_18_Characters_as_Job_Ref__c") != null
					&& sobj.getField(
							""+ns+"Use_18_Characters_as_Job_Ref__c")
							.toString().equalsIgnoreCase("true")) {
				adDigit = 18;
			}

			if (sobj.getChild(""+ns+"Vacancy__r").getField("Id") == null) {
				logger.error(sforg.getSfusername() + ": Advertisement record "
						+ sobj.getChild("Id").getValue().toString()
						+ "'s vacancy has been deleted.");
				// if the vacancy is deleted
				vacanciesMap.put(sobj.getId().toString().substring(0, adDigit),
						null);
			} else {
				// ad Id as key, vacancy Id as value
				vacanciesMap.put(sobj.getId().toString().substring(0, adDigit),
						sobj.getChild(""+ns+"Vacancy__r")
								.getField("Id").toString());
			}

			// associate ad id with selected skill groups
			if (skillParsingEnabled
					&& sobj.getChild(""+ns+"Skill_Group_Criteria__c")
							.getValue() != null) {
				String[] tempSkills = sobj
						.getChild(""+ns+"Skill_Group_Criteria__c")
						.getValue().toString().split(",");
				skillGroupsCriteriaMap.put(
						sobj.getId().toString().substring(0, adDigit),
						Arrays.asList(tempSkills));
			} else if (skillParsingEnabled
					&& sobj.getChild(""+ns+"Skill_Group_Criteria__c")
							.getValue() == null) {
				// if the skill group criteria under the ad record doesn't exist
				logger.warn(sforg.getSfusername() + ": advertisement "
						+ sobj.getChild("Id").getValue().toString()
						+ " does not have skill groups criteria.");
			}
		}

		// set vid and skill group criteria for candidates
		for (Candidate can : successCandidateList) {
			String tmpCandidateAdId = can.getAdvertisementId();
			can.setVacancyId(vacanciesMap.get(tmpCandidateAdId));
			can.setSkillGroupsList(skillGroupsCriteriaMap.get(tmpCandidateAdId));
		}
		logger.debug("end of retrieving vacancy info ...");
	}

}

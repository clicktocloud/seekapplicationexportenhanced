package com.ctc.jobboard.sae.export.util;

import com.ctc.jobboard.util.BasicConfig;


public class SAEExportConfig {
	
	
	public static String get(String name){
		return BasicConfig.get(name);
	}
	
	public static String getNamespace(){
		return get("namespace");
	}
	
}

package com.ctc.jobboard.sae.export.service;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ctc.jobboard.sae.component.SAEAccount;
import com.ctc.jobboard.sae.component.SAECandidatesRetriever;
import com.ctc.jobboard.sae.component.SAEJobBoard;
import com.ctc.jobboard.sae.export.domain.SAEOrg;
import com.ctc.jobboard.sae.export.helper.SalesforceHelper;
import com.ctc.jobboard.sae.export.helper.SalesforceService;
import com.ctc.jobboard.util.SfOrg;

public class SaeTaskDispatcher {
	
	static Logger logger = Logger.getLogger("com.ctc.seek.export");
	
	public void dispatchTasks(List<SAEOrg> orgsList){
		while(!orgsList.isEmpty() && orgsList != null){

            try{
                // to get a single client from the list.
            	SAEOrg sforg = SalesforceHelper.getSingleOrg(orgsList);
                if(sforg == null)
                    break;

                logger.info("---------------	start for username = "+sforg.getSfusername() + "	-------------------");
                List<SAEAccount> seekAccountsConfigList = SalesforceService.getAllSeekAccountConfigurations(sforg);
                if(seekAccountsConfigList==null || seekAccountsConfigList.size()==0){
                    continue;
                }
               
                		
                for(SAEAccount account: seekAccountsConfigList){
                	SAEJobBoard jobboard = new SAEJobBoard("seek");
                	jobboard.setAccount(account);
                	jobboard.setOperator(account.getOrgUsername());
                	jobboard.setBucket(account.getBucket());
                	jobboard.setCurrentSforg(new SfOrg(account.getOrgId()));
                	jobboard.makeJobFeeds();
                    CandidatesRetriever saeCR = new SaeCandidatesRetriever(sforg,account);
                    saeCR.retrieveCandidates();
                }
                
                logger.info("---------------	end for username = "+sforg.getSfusername() + "	-------------------");
            } catch(Exception ex){
                continue;
            }

		}
	}	
}

package com.ctc.jobboard.sae.export.domain;

public class SAEOrg {
	private String id15;
	private String id18;
	private String name;
	private String sfusername;
	private String s3accesskey;
	private String s3secretkey;
	private String s3Folder;
	private Integer totalLicenseNumber;
    private boolean uploadOtherDocEnabled;
    private boolean use18DigitAdIdForJobRefNumber;

    public boolean isUse18DigitAdIdForJobRefNumber() {
		return use18DigitAdIdForJobRefNumber;
	}

	public void setUse18DigitAdIdForJobRefNumber(
			boolean use18DigitAdIdForJobRefNumber) {
		this.use18DigitAdIdForJobRefNumber = use18DigitAdIdForJobRefNumber;
	}

    public boolean isUploadOtherDocEnabled() {
        return uploadOtherDocEnabled;
    }

    public void setUploadOtherDocEnabled(boolean uploadOtherDocEnabled) {
        this.uploadOtherDocEnabled = uploadOtherDocEnabled;
    }


	public String getS3Folder() {
		return s3Folder;
	}
	public void setS3Folder(String s3Folder) {
		this.s3Folder = s3Folder;
	}
	public String getId15() {
		return id15;
	}
	public void setId15(String id15) {
		this.id15 = id15;
	}
	public String getId18() {
		return id18;
	}
	public void setId18(String id18) {
		this.id18 = id18;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSfusername() {
		return sfusername;
	}
	public void setSfusername(String sfusername) {
		this.sfusername = sfusername;
	}
	public String getS3accesskey() {
		return s3accesskey;
	}
	public void setS3accesskey(String s3accesskey) {
		this.s3accesskey = s3accesskey;
	}
	public String getS3secretkey() {
		return s3secretkey;
	}
	public void setS3secretkey(String s3secretkey) {
		this.s3secretkey = s3secretkey;
	}
	public void setTotalLicenseNumber(String totalLicenseNumber) {
		Float totalLicenseNumber1 = Float.parseFloat(totalLicenseNumber);
		this.totalLicenseNumber = totalLicenseNumber1.intValue();
	}
	public Integer getTotalLicenseNumber() {
		return totalLicenseNumber;
	}
	@Override
	public String toString() {
		return "Sforg [id15=" + id15 + ", id18=" + id18 + ", name=" + name
				+ ", sfusername=" + sfusername + ", s3accesskey=" + s3accesskey
				+ ", s3secretkey=" + s3secretkey + ", totalLicenseNumber=" + totalLicenseNumber + "]";
	}
	
}

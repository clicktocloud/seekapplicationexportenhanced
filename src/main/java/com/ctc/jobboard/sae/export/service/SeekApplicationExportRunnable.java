package com.ctc.jobboard.sae.export.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ctc.jobboard.sae.export.domain.SAEOrg;


public class SeekApplicationExportRunnable implements Runnable {
	private List<SAEOrg> allSAEOrgs = new ArrayList<SAEOrg>();
	
	static Logger logger = Logger.getLogger(SeekApplicationExportRunnable.class);

	public SeekApplicationExportRunnable(List<SAEOrg> allOrgs) {
		this.allSAEOrgs = allOrgs;
		
	}

	public void run() {
		logger.info("start to run thread:" + Thread.currentThread().getName());
		long startTime = System.currentTimeMillis();

		SaeTaskDispatcher sae = new SaeTaskDispatcher();
		
		sae.dispatchTasks(allSAEOrgs);
		
		long endTime = System.currentTimeMillis();
		logger.info("total time consumed for "
				+ Thread.currentThread().getName() + ": "
				+ (endTime - startTime) / 1000 + "s");
	}
	
	
}

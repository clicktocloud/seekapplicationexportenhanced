package com.ctc.jobboard.sae.export.persistence.drivers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ctc.jobboard.component.BasicAccount;
import com.ctc.jobboard.component.IAccount;
import com.ctc.jobboard.persistence.JBConnection;
import com.ctc.jobboard.persistence.JBConnectionException;
import com.ctc.jobboard.persistence.JBConnectionManager;
import com.ctc.jobboard.persistence.JBField;
import com.ctc.jobboard.persistence.JBObject;
import com.ctc.jobboard.util.BasicConfig;
import com.ctc.jobboard.util.PartitionUtil;
import com.ctc.jobboard.util.SfOrg;
import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

public class SfJBConnection implements JBConnection {
	
	private final static Integer BULK_SIZE ;
	
	static{
		JBConnectionManager.register(SfJBConnection.class);
		
		int sizeint = 12;
		String size = BasicConfig.get("bulk_save_size");
		if(StringUtils.isEmpty( size )){
			size = "12";
		}
		
		
		try {
			sizeint = Integer.valueOf(size);
		} catch (NumberFormatException e) {
			
		}
		
		BULK_SIZE = sizeint;
		
	}
	
	private PartnerConnection conn;
	
	private String orgId;
	
	private Object connlock = new Object();
	
	public SfJBConnection(String orgId) {
		super();
		this.orgId = orgId;
	}

	@Override
	public SfJBConnection connect() throws JBConnectionException {
		if(StringUtils.isEmpty(orgId)){
			return null;
		}
		synchronized (connlock) {
			if(conn == null){
				try {
					conn = SalesforceAgent.getConnectionByOAuth(orgId);
				} catch (ConnectionException e) {
					throw new JBConnectionException(e);
				}
			}
			
		}
		return this;
		
	}

	@Override
	public void update(List<JBObject> jbObjects) throws JBConnectionException {
		synchronized (connlock) {
			if (conn == null) {
				connect();
			}
		}
		
		if(conn == null){
			
			throw new JBConnectionException("Failed to get connectio to salesforce instance ["+orgId+"]");
		}
		
		if(jbObjects == null || jbObjects.size() == 0){
			return;
		}
		
		JBConnectionException exception = null;
		
		//mapping JBObject to SObject
		List<SObject> sfobjects = convert(jbObjects);
		
		
		//update sobjects to salesforce
		List<List<SObject>> sobjsList = PartitionUtil.partition(sfobjects, BULK_SIZE);
		for(List<SObject> sobjs : sobjsList){
			SaveResult[] srs = null;
			try {
				srs = conn.update(sobjs.toArray(new SObject[]{}));
				for(SaveResult sr:srs){
					if(!sr.getSuccess()){
						for(com.sforce.soap.partner.Error error : sr.getErrors()){
							if(exception == null){
								exception = new JBConnectionException("Failed to execute updating to salesforce");
							}
							exception.addErrors(new JBConnectionException.Error(error.getMessage(), error.getFields()));
							
						}
					}
				}
			} catch (ConnectionException e) {
				exception = new JBConnectionException(e);
			}
			
		}
		
		if(exception != null){
			throw exception;
		}

	}
	
	

	
	public void setConn(PartnerConnection conn) {
		this.conn = conn;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public List<SObject> convert(Collection<JBObject> jbObjects){
		List<SObject> sfobjects = new ArrayList<SObject>();
		for(JBObject jbo : jbObjects){
			SObject sobject = new SObject();
			sobject.setType(jbo.getObjectName());
			for(JBField field : jbo.getFields()){
				sobject.setField(field.getName(), field.getValue());
			}
			
			sfobjects.add(sobject);
			
		}
		
		return sfobjects;
	}

	@Override
	public PartnerConnection getConn() {
		
		return conn;
	}

	@Override
	public <T> T query(String queryString, Class<T> resultClass) throws JBConnectionException {
		synchronized (connlock) {
			if (conn == null) {
				connect();
			}
		}
		
		if(conn == null){
			
			throw new JBConnectionException("Failed to get connectio to salesforce instance ["+orgId+"]");
		}
		
		try {
			return resultClass.cast(conn.query(queryString));
		} catch (ConnectionException e) {
			throw new JBConnectionException(e);
		}
	}

	@Override
	public <T> T queryMore(String queryString, Class<T> resultClass) 
			throws JBConnectionException {
		
		try {
			return resultClass.cast( conn.queryMore(queryString) );
		} catch (ConnectionException e) {
			throw new JBConnectionException(e);
		}
	}
	
	@Override
	public IAccount getAccount( String jobBoardName, String namespace) throws JBConnectionException{
		IAccount account = null;

		String queryString  =  "Select "+
				namespace+"Advertisement_Account__c, "+
				namespace+"UserName__c,  "+
				namespace+"Password__c " +
				" From "+
				namespace+"Advertisement_Configuration__c " +
				" where "+
				namespace+"Advertisement_Account__c!=null and "+
				namespace+"UserName__c!=null and "+
				namespace+"Password__c!=null and "+
				namespace+"Active__c=true and "+
				namespace+"WebSite__c='"+jobBoardName+"' limit 1";
				
		QueryResult qr = query(queryString,QueryResult.class);
		
		account = new BasicAccount() ;
		if(qr.getSize()>0){
			SObject sobj = qr.getRecords()[0];
			account.setAdvertiserId(sobj.getField(namespace+"Advertisement_Account__c").toString());
			account.setUsername(sobj.getField(namespace+"UserName__c").toString());
			account.setPassword(sobj.getField(namespace+"Password__c").toString());
		}else{
			throw new JBConnectionException("this org has no username, password or advertiser id for "+jobBoardName);
		}
		
		
		return account;
	}
	
	public  List<SfOrg>  getOrgList( String jobBoardSfApiName ) throws JBConnectionException{
		
		
		List<SfOrg> list = new ArrayList<SfOrg>();
		try {
			PartnerConnection connection = SalesforceAgent.getConnectionByOAuth(BasicConfig.CORP_ORG_ID);
			
			QueryResult qr = connection.query(  getOrgListQueryString( jobBoardSfApiName ) );
			boolean done = false;
			if(qr.getSize()>0){
				while(!done){
					for(SObject sobj: qr.getRecords()){
						
						//only for test nest org
						String orgId = (String)sobj.getField("SFDC_Org_ID__c");
						
						if(BasicConfig.isTest){
							if(BasicConfig.testOrgIds.contains(orgId)){
								list.add(new SfOrg(orgId));
							}
						}else{
							list.add(new SfOrg(orgId));
						}	
						
					}
					if(qr.isDone()){
						done = true;
					}else{
						qr = connection.queryMore(qr.getQueryLocator());
					}
				}
			}
		} catch (ConnectionException e) {
			
			throw new JBConnectionException(e);
		}
		return list;

	}
	
	public static String getOrgListQueryString(String jobBoardSfApiName){
		
		return "select SFDC_Org_ID__c, SF_UserName__c from Account " +
				 " where "+jobBoardSfApiName+"=true and PeopleCloud_Platform__c=true";
	}
	public static String getJobContentQueryString( String jobBoardName, String namespace){
		return "select Id, "+
				namespace+"Job_Posting_Status_txt__c, "+
				namespace+"Placement_Date__c, "+
				namespace+"JobXML__c, " + 
				namespace+"Job_Posting_Status__c " +
				" from "+
				namespace+"Advertisement__c " +
				" where "+
				namespace+"WebSite__c='"+jobBoardName+"' and "+
				namespace+"Status__c='Active'";
	}

}

package com.ctc.jobboard.sae.export.exeption;

public class SeekApplicationException extends RuntimeException {

	private static final long serialVersionUID = 428360874953501830L;

	public SeekApplicationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SeekApplicationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SeekApplicationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SeekApplicationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}

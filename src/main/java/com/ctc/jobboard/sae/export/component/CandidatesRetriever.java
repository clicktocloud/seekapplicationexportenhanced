package com.ctc.jobboard.sae.export.component;

import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.jobboard.sae.component.SAEAccount;
import com.ctc.jobboard.sae.domain.Candidate;
import com.ctc.jobboard.sae.domain.SeekScreenIdConfig;
import com.ctc.jobboard.sae.export.domain.SAEOrg;

/**
 * Created with IntelliJ IDEA.
 * User: kevin
 * Date: 6/25/13
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class CandidatesRetriever {
    SAEAccount seekAccount;
    SAEOrg sforg;
    
    static Logger logger = Logger.getLogger(CandidatesRetriever.class);
    static final int STEP =25;

    public CandidatesRetriever(SAEOrg org,SAEAccount account){
        seekAccount = account;
        sforg = org;
       
    }

  
    abstract List<Candidate> fetchCandidatesFromSeek(SAEOrg sforg, String seekAccontId, String seekMaxCandidateId) throws Exception;
    abstract void downloadScreen(List<Candidate>  candidates,  List<SeekScreenIdConfig> listSeekScreenIdConfig);
    abstract void downloadResumesCoverLettersAndSelectionCriteria(List<Candidate> candidates,  String s3Folder, String sfUsername);
   
    abstract void importCandidates(List<Candidate> candidates);


    // template method
    public final void retrieveCandidates(){
        logger.info("Start to handle account:" + seekAccount.getAccountId() + " -----> max Job Application Id:" + seekAccount.getOrgMaxContactid());
        try {
           
           
            // pull all candidates from Seek
            logger.debug(seekAccount.getAccountId());
            List<Candidate> candidates = fetchCandidatesFromSeek( sforg, seekAccount.getAccountId(), seekAccount.getOrgMaxContactid());
            logger.info("Total candidates to export:"+candidates.size());
         
            
            if(candidates.size() > 0) {
                int count = 0; // count batches 
                int startIndex = 0;
                int endIndex =0;
                logger.info("Divide candidates into batches and process(Parse resume and create SF records). "+STEP+" in a batch");
                do{
                	count++;
                	logger.info("Processing batch number:"+ count);
                	// Divide candidates into batches and process (number of candidate in a batch is equal to STEP) 
                	List<Candidate> processList;
                	if(startIndex+STEP>=candidates.size()){
                		endIndex = candidates.size();
                		processList = candidates.subList(startIndex, endIndex);
                		startIndex = endIndex;
                	}else{
                		endIndex = startIndex + STEP;
                		processList = candidates.subList(startIndex, endIndex);
                		startIndex = endIndex;
                	}
                	
                    // download questions & answers
                    downloadScreen(processList, seekAccount.getListSeekScreenIdConfig());

                    // download the resume for all the candidates per batch.
                    downloadResumesCoverLettersAndSelectionCriteria(processList, sforg.getS3Folder(), sforg.getSfusername());

                   
                   importCandidates(processList);

                }while(startIndex<candidates.size());
            }
        }catch (Throwable ex){
            logger.error("Errors when handling " + sforg.getSfusername() + "'s seek account"+ seekAccount.getAccountId() + ":" + ex );
        }
        logger.info("Complete handling account:" + seekAccount.getAccountId() + " -----> max Job Application Id:" + seekAccount.getOrgMaxContactid());
    }

}
